package VIMx::autoload::ducttape::frontmatter;

use v5.10;
use strict;
use warnings;

use VIMx::Symbiont;
use YAML::Tiny;

# this could use smartening
func exists => sub { buf_has_frontmatter($a{000} ? $BUFFERS{$a{0}} : $BUFFER) };

func args => 'dict', set => sub { buf_write_frontmatter($a{dict}, $BUFFER) };

sub buf_write_frontmatter {
    my ($dict, $buf) = @_;
    $buf //= $BUFFER;

    my $yaml = YAML::Tiny->new($dict)->write_string;

    buf_del_frontmatter($buf);

    unshift @$buf, split(/\n/, $yaml), '---';
    return;
}

func args => q{key, value}, setkey => sub {

    ...
};

func get => sub {

    if (!buf_has_frontmatter($BUFFER)) {
        return undef if $a{000};
        return {};
    }

    my $yaml_text = q{};

    my $i = 1;
    while (my $line = $BUFFER->[$i++]) {
        $yaml_text .= "\n$line";
        last if $line eq '---'
    }

    # TODO catch this on fails
    ### $yaml_text
    my $yaml = YAML::Tiny->read_string($yaml_text);
    my $fm = $yaml->[0] // {};

    ### $yaml
    return $a{000} ? $fm->{$a{0}} : $fm;
};

function args => q{}, remove => sub { buf_del_frontmatter($BUFFER) };

func0 clear => sub {
    buf_del_frontmatter($BUFFER);
    unshift @$BUFFER, '---', '---';
    return undef;
};

function args => q{}, clear_values => sub { ... };

# this could use smartening
sub buf_has_frontmatter {
    my $buf = shift;
    return $buf->[0] eq '---' ? 1 : 0;
}

sub buf_del_frontmatter {
    my $buf = shift || $BUFFER;

    return
        unless buf_has_frontmatter($buf);

    shift @$buf;

    while (my $line = shift @$buf) {
        last if $line eq '---'
    }

    return;
}

!!42;
